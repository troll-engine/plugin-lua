# EARLY WORK - NOT YET USABLE

<div align="center">
<p align="center"><img src="https://gitlab.com/troll-engine/plugin-lua/raw/master/logo.svg" align="center" width="350" height="250" alt="Project icon"></p>

<blockquote>
<p align="center">Lua language plugin for Troll Engine</p>
</blockquote>

<p align="center">
  <a href="https://gitlab.com/troll-engine/plugin-lua/commits/master" alt="pipeline status"><img src="https://gitlab.com/troll-engine/plugin-lua/badges/master/pipeline.svg" /></a>
  <a href="https://gitlab.com/troll-engine/plugin-lua/commits/master" alt="coverage report"><img src="https://gitlab.com/troll-engine/plugin-lua/badges/master/coverage.svg" /></a>
  <a href="https://www.npmjs.com/package/@troll-engine/plugin-lua" alt="version"><img src="https://badgen.net/npm/v/@troll-engine/plugin-lua" /></a>
  <a href="https://www.npmjs.com/package/@troll-engine/plugin-lua" alt="license"><img src="https://badgen.net/npm/license/@troll-engine/plugin-lua" /></a>
  <a href="https://www.npmjs.com/package/@troll-engine/plugin-lua" alt="downloads"><img src="https://badgen.net/npm/dt/@troll-engine/plugin-lua" /></a>
  <a href="https://discord.gg/ewwaSxv" alt="discord"><img src="https://discordapp.com/api/guilds/621750483098271745/widget.png" /></a>
</p>

</div>
</p>

```bash
npm install --save @troll-engine/plugin-lua
```
